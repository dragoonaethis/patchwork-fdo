# Patchwork - automated patch tracking system
# Copyright (C) 2020 Arkadiusz Hiler <arkadiusz.hiler@intel.com>
#
# This file is part of the Patchwork package.
#
# Patchwork is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Patchwork is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Patchwork; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from patchwork.models import Patch, Series
from .test_series import Series0010


class SeriesMsgIDLookup(Series0010):
    url = '/series/msgid/{}/'

    def series(self):
        return Series.objects.get()

    def testCoverLetterRedirects(self):
        response = self.client.get(self.url.format(self.root_msgid))
        self.assertRedirects(response, self.series().get_absolute_url())

    def testPatchMsgIdRedirects(self):
        msg_id = Patch.objects.first().msgid
        response = self.client.get(self.url.format(msg_id))
        self.assertRedirects(response, self.series().get_absolute_url())

    def testNoAngleBracketsRedirects(self):
        msg_id = Patch.objects.first().msgid
        msg_id = msg_id.strip("<>")
        response = self.client.get(self.url.format(msg_id))
        self.assertRedirects(response, self.series().get_absolute_url())

    def testWrongMsgId404(self):
        msg_id = "toto@tata"
        response = self.client.get(self.url.format(msg_id))
        self.assertEqual(404, response.status_code)


class PatchMsgIDLookup(Series0010):
    url = '/patch/msgid/{}/'

    def patch(self):
        return Patch.objects.first()

    def testPatchMsgIdRedirects(self):
        msg_id = self.patch().msgid
        response = self.client.get(self.url.format(msg_id))
        self.assertRedirects(response, self.patch().get_absolute_url())

    def testNoAngleBracketsRedirects(self):
        msg_id = self.patch().msgid
        msg_id = msg_id.strip("<>")
        response = self.client.get(self.url.format(msg_id))
        self.assertRedirects(response, self.patch().get_absolute_url())

    def testWrongMsgId404(self):
        msg_id = "toto@tata"
        response = self.client.get(self.url.format(msg_id))
        self.assertEqual(404, response.status_code)
