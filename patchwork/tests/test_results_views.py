# Patchwork - automated patch tracking system
# Copyright (C) 2020 Arkadiusz Hiler <arkadiusz.hiler@intel.com>
#
# This file is part of the Patchwork package.
#
# Patchwork is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Patchwork is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Patchwork; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from lxml import html

from patchwork.models import (Test, TestResult, TestState,
                              SeriesRevision, Series)
from patchwork.tests.utils import create_user

from .test_series import Series0010


class RenderingModes(Series0010):
    fixtures = ['default_states', 'default_events']
    SERIES_RESULTS_URL = '/series/{}/'

    def setUp(self):
        super(Series0010, self).setUp()
        self.series = Series.objects.get()
        self.series_revision = SeriesRevision.objects.get()
        self.test = Test(project=self.project, name="some.test")
        self.test.save()

        self.test_result = TestResult(test=self.test,
                                      revision=self.series_revision,
                                      user=create_user(),
                                      state=TestState.STATE_SUCCESS)
        self.test_result.save()

    def test_markdown(self):
        self.test_result.summary = "[foo](http://example.com)"
        self.test_result.save()
        self.test.is_markdown = True
        self.test.save()

        resp = self.client.get('/series/%s/' % self.series.pk)
        self.assertEquals(resp.status_code, 200)
        content = resp.content.decode()
        tree = html.fromstring(content)

        self.assertTrue(tree.xpath("//a[@href='http://example.com']"))
        self.assertFalse(self.test_result.summary in content)

    def test_no_markdown(self):
        self.test_result.summary = "[foo](http://example.com)"
        self.test_result.save()
        self.test.is_markdown = False
        self.test.save()

        resp = self.client.get('/series/%s/' % self.series.pk)
        self.assertEquals(resp.status_code, 200)
        content = resp.content.decode()
        tree = html.fromstring(content)

        self.assertFalse(tree.xpath("//a[@href='http://example.com']"))
        self.assertTrue(self.test_result.summary in content)
