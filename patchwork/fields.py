# Patchwork - automated patch tracking system
# Copyright (C) 2008 Jeremy Kerr <jk@ozlabs.org>
# Copyright (C) 2015 Intel Corporation
#
# This file is part of the Patchwork package.
#
# Patchwork is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Patchwork is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Patchwork; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from __future__ import absolute_import

import hashlib

from django.db import models


class CaseInsensitiveCharField(models.CharField):
    CONVERTIBLES = ('exact', 'exact', 'contains', 'startswith',
                    'endswith', 'regex')

    def get_lookup(self, lookup_name):
        if lookup_name in self.CONVERTIBLES:
            lookup_name = f"i{lookup_name}"
        return super().get_lookup(lookup_name)

    def pre_save(self, model_instance, add):
        attr = super().pre_save(model_instance, add)
        attr = attr.lower()
        setattr(model_instance, self.attname, attr)
        return attr


class HashField(models.CharField):

    def __init__(self, *args, **kwargs):
        self.n_bytes = len(hashlib.sha1().hexdigest())
        kwargs['max_length'] = self.n_bytes

        super(HashField, self).__init__(*args, **kwargs)

    def construct(self, value):
        if isinstance(value, str):
            value = value.encode('utf-8')
        return hashlib.sha1(value)

    def from_db_value(self, value, expression, connection, context=None):
        return self.to_python(value)

    def db_type(self, connection=None):
        return 'char(%d)' % self.n_bytes
