ARG UWSGI_CONTAINER=docker.io/tiangolo/uwsgi-nginx:python3.10
FROM $UWSGI_CONTAINER

LABEL maintainer="Martin Roukala <martin.roukala@mupuf.org>"

# Copy the website to the /app folder
COPY . /app/
COPY .gitlab-ci/settings-production.py /app/patchwork/settings/production.py

# Install all our dependencies
WORKDIR /app
RUN pip install -r docs/requirements-prod-postgres.txt
